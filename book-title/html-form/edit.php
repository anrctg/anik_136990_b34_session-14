<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Edit - Book title</title>
    <link rel="stylesheet" href="../../asset/bootstrap/css/bootstrap.min.css">
    <script src="../../asset/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="../../asset/bootstrap/fonts/">
</head>
<style>
    body{
        padding-top: 50px;
    }
    fieldset.scheduler-border {
        border: 1px groove #ddd !important;
        padding: 0 1.4em 1.4em 1.4em !important;
        margin: 0 0 1.5em 0 !important;
        -webkit-box-shadow:  0px 0px 0px 0px #000;
        box-shadow:  0px 0px 0px 0px #000;
    }

    legend.scheduler-border {
        font-size: 1.2em !important;
        font-weight: bold !important;
        text-align: left !important;
        width:auto;
        padding:0 10px;
        border-bottom:none;
    }
    .anr-panel-form{
        padding: 10px;
    }
</style>
<body>


<div class="container">

<div class="col-sm-4 col-sm-offset-4">

    <div class="panel panel-primary anr-panel-form">
        <div class="panel-heading text-centerx">Panel heading</div>
<form action="" class="form">
    <div class="form-group">
    <label for="title">Book Title</label>
    <input type="text" class="form-control" name="book_title" placeholder="Enter book title">
    </div>
    <div class="form-group">
    <label for="book_author">Book Author</label>
    <input type="text" class="form-control" name="book_author" placeholder="Add author name">
    </div>

    <button type="submit" class="btn btn-success btn-md btn-block"><span class="glyphicon glyphicon-pencil"></span> Edit Now</button>

</form>
    </div>
</div>
</div>

</body>
</html>
